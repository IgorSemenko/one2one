<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class CompanyController extends Controller

{
    public function index(){
        return view('company.index');
    }
    
    public function addJobSubmit(Request $r){
        
        if($r->skills){
            $skills = implode(',', $r->skills);
        }else{
            $skills = null;
        }        
        
        $title = $r->job_title;
        
        $requirements = $r->requirements;
        
        $contact_email = $r->contact_email;
        
        $add_job=DB::table('jobs')->insert([
           'company_id' => Auth::user()->id,
           'skills' => $skills,
           'job_title' => $title,
           'requirements' => $requirements,
           'contact_email' => $contact_email,            
        ]);
        
        if($add_job){
            
            $jobs = DB::table('jobs')
                 ->where('company_id', Auth::user()->id)
                 ->get();
         
            return view('company.jobs', compact('jobs', $jobs));  
        }       
       
    }
    
     public function viewJobs(){
         
         $jobs = DB::table('jobs')
                 ->where('company_id', Auth::user()->id)
                 ->get();
         
         return view('company.jobs', compact('jobs', $jobs));         
         
     }
}
