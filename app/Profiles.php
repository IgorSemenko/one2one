<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Profiles extends Model
{
    protected $fillable = ['city', 'country', 'about', 'contact', 'users_id'];
    
//    public function profile(){
//        
//        return $this->hasOne('App\Profiles');
//    }
    
    public static function user()
        {
          return $this->belongsTo('App\User');
        }
}
