@extends('profile.master')

@section('content')
<div class="container">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{url('/profile').'/'. Auth::user()->name}}">Profile</a></li>
            <li class="breadcrumb-item"><a href="{{url('/findFriens')}}">Frind friends</a></li>
        </ol>
    </nav>

    <div class="row">
        
        @include('profile.sidebarleft')
        
        <div class="col-md-10">
            <div class="row justify-content-center">

                <div class="col-md-12">
                    
                    <div class="card">
                        
                        <div class="card-header">{{ucwords( Auth::user()->name )}} find a friends
                           
                        </div>
                        <?php //print_r($allusers) ?>
                        <div class="card-body">
                            <div class="row"> <h6 class="col-md-12">All users:</h6></div>
                                                        
                            @foreach($allusers as $user)
                            
                            <div class="col-md-4 find-user">
                                <div class="img-thumbnail"> 
                                    <h3><a href="{{url('/profile')}}/{{$user->name}}">{{ucwords($user->name)}}</a></h3>
                                    <a href="{{url('/profile')}}/{{$user->name}}"><img src="{{URL::asset('users/img/' . $user->foto)}}" alt='profile photo' height="140" width="120"></a>
                                <div class="caption">
                                    <h5>City: {{ucwords($user->city)}}</h5>
                                    <h6>Age: {{ucwords($user->age)}}</h6>
                                    
                                    <?php $check = DB::table('friendships')
                                            ->where('user_requested', '=', $user->users_id)
                                            ->where('requester', '=', Auth::user()->id)
                                            ->first();
                                    ?>
                                    
                                    <?php if(!$check): ?> 
                                    
                                        <p><a class="btn-sm btn-info" href="{{url('/')}}/addFriend/{{$user->users_id}}">add to friends</a></p>
                                    
                                            <?php else : ?>
                                        
                                        <p>request already send</p>
                                        
                                    <?php endif ?>
                                    
                                </div>
                                    </div>
                            </div>                           
                            
                            @endforeach                           
                            
                        </div>
                        
                    </div>
                </div>

                
                </div>
                
                
            </div>
        </div>
    </div>
    @endsection
