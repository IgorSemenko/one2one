<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MessagerController extends Controller {

    public function getMessages() {

        if (Auth::check()) {

            $u_id = Auth::user()->id;

            $allUsers = DB::table('users')
                    ->where('id', '!=', Auth::user()->id)
                    ->get();

            return $allUsers;
        } else {
            return view('welcome');
        }
    }

    public function getUserMessages($id) {

        if (Auth::check()) {
            $u_id = Auth::user()->id;
        } else {
            return view('welcome');
        }



        $converOne = DB::table('conversation')
                ->where('user_one', $u_id)
                ->Where('user_two', $id)
                ->get();

        $converTwo = DB::table('conversation')
                ->where('user_one', $id)
                ->Where('user_two', $u_id)
                ->get();

        $test = array_merge($converOne->toArray(), $converTwo->toArray());
        //dd($test);

        if ($test) {

            $conver = $test[0]->id;

            $dialog = DB::table('messages')
                    ->Join('users', 'users.id', 'messages.user_from')
                    ->where('conversation_id', $conver)
                    ->get();

            return $dialog;
        } else {
            echo 1;
        }

    }

    public function sendMsg(Request $req) {

        if (Auth::check()) {
            $u_id = Auth::user()->id;
        } else {
            return view('welcome');
        }

        $msg = $req->msg;

        $conId = $req->conId;

        if (isset($req->conId)) {

            $fetchUser = DB::table('messages')
                    ->where('conversation_id', $conId)
                    ->where('user_to', '!=', $u_id)
                    ->get();

            $userTo = $fetchUser[0]->user_to;

            $sendMes = DB::table('messages')
                    ->insert([
                'user_to' => $userTo,
                'user_from' => $u_id,
                'msg' => $msg,
                'status' => 1,
                'conversation_id' => $conId
            ]);

            if ($sendMes) {

                $dialog = DB::table('messages')
                        ->Join('users', 'users.id', 'messages.user_from')
                        ->where('conversation_id', $conId)
                        ->get();

                return $dialog;
            }
        } else {

            $newCon = DB::table('conversation')
                    ->insertGetId([
                'user_one' => $u_id,
                'user_two' => $req->user2
            ]);

            if (isset($newCon)) {

                $saveNewMess = DB::table('messages')
                        ->insert([
                    'user_to' => $req->user2,
                    'user_from' => $u_id,
                    'msg' => $msg,
                    'status' => 1,
                    'conversation_id' => $newCon
                ]);
            }

            if (isset($saveNewMess)) {
                $dialog = DB::table('messages')
                        ->Join('users', 'users.id', 'messages.user_from')
                        ->where('conversation_id', $conId)
                        ->get();

                return $dialog;
            }
        }
    }

}
