@extends('profile.master')

@section('content')

<div class="container">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{url('/profile').'/'. Auth::user()->name}}">Profile</a></li>
        </ol>
    </nav>
    <div class="row">
        
         @include('profile.sidebarleft')
         
        <div class="col-md-10">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                       
                        @foreach($userData as $uData)
                        
                        <div class="card-header">{{ucwords( $uData->name )}}</div>

                        <div class="card-body">
                            @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                            @endif
                            
                        </div>
                        
                         <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <img src="{{URL::asset('users/img/' . $uData->foto)}}" alt='profile photo' height="120" width="100">                                            
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="row"> 
                                        <div class="col-md-12">
                                            <p><label>From</label></p>
                                            <p>{{$uData->city}}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p><lable>About </lable></p>
                                            <p>{{$uData->about}}</p>
                                        </div>
                                    </div>
                                </div>
                        
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
