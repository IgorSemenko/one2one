@extends('profile.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ucwords( Auth::user()->name )}}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You can change your foto!
                </div>
                
                <img src="{{URL::asset('users/img/' . Auth::user()->foto)}}" alt='profile photo' height="140" width="120">
                
                <hr>
                
                <form action="{{URL('/')}}/uploadFoto" method="post"  enctype="multipart/form-data">
                    
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    
                    <input type="file" name="foto" class="form-control" />
                    
                    <input type="submit" class="btn button-success" name="btn"/>
                    
                </form> 
                 
            </div>
        </div>
    </div>
</div>
@endsection
