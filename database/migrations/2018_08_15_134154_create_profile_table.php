<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');            
            $table->string('city')->nullable;
            $table->string('county')->nullable;
            $table->string('about')->nullable;
            $table->string('contact')->nullable;
            $table->rememberToken();
            $table->timestamps();
        });
        
        Schema::table('profiles', function (Blueprint $table) {
            
            $table->unsignedInteger('users_id');

            $table->foreign('users_id')->references('id')->on('users');
        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
