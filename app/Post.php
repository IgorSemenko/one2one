<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Like;

class Post extends Model
{
    protected $guarded = [];

    public function users(){
        
        return $this->belongsTo('App\User', 'user_id');
    }
    
    public function likes(){
        
        return $this->hasMany('App\Like', 'post_id');
    }
}
