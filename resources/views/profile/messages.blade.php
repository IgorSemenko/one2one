@extends('profile.mainMessages')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-3 left_part">
            <div class="panel panel-default">
                <div class="panel-heading">Click on user:</div>
                <ul v-for='private in privateMsg' class="private_ul">
                    <li @click='messages(private.id)' class="private_list">
                        <img :src="'http://one2one.com/users/img/' + private.foto" class ='msg-foto'> 
                            @{{private.name}}
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-md-7 center_part">
            <div class="panel panel-default">
                <div class="panel-heading"><h5>Messages:</h5></div>                

                <div v-for="singleMsg in singleMsgs" class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div v-if="singleMsg.user_from == <?php echo Auth::user()->id; ?>" class="text_block"> 

                                <div class="container">
                                    <div class="row header_mes">
                                        <div class="col-md-6 head_name">
                                            <img :src="'http://one2one.com/users/img/' + singleMsg.foto" class ='msg-foto'>
                                                <span>@{{singleMsg.name}}</span>
                                        </div>
                                        <div class="col-md-6">
                                            @{{singleMsg.created_at}}
                                        </div>
                                    </div>

                                </div>

                                <p class="message"> @{{singleMsg.msg}}</p>

                            </div>

                            <div v-else class="answer text_block">

                                <div class="container">
                                    <div class="row header_mes">
                                        <div class="col-md-6 head_name">
                                            <img :src="'http://one2one.com/users/img/' + singleMsg.foto" class ='msg-foto'>
                                                <span>@{{singleMsg.name}}</span>
                                        </div>
                                        <div class="col-md-6">
                                            @{{singleMsg.created_at}}
                                        </div>
                                    </div>

                                </div>
                                <p class="message">@{{singleMsg.msg}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <input v-model="user2" type="hidden" value="new_mess">
                <input v-model="conId" type="hidden" value="new_mess">
                <div class="container mess">
                    <div>write new message</div>
                    <textarea class="form-control new_mess" v-model="msgFrom" @keydown="inputHandler"></textarea>
                </div>
            </div>
        </div>

        <div class="col-md-2 left_part">
            <div class="panel panel-default">
                <div class="panel-heading">A Basic Panel</div>
            </div>
        </div>

    </div>
</div>
@endsection

