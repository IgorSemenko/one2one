@extends('profile.master')

@section('content')

<div class="container">


    <div class="row">

        <div class="container">
            <div class="row">
                <div class="col-md-12 bread">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/posts')}}">Home</a></li>

                        </ol>
                    </nav>
                </div>
            </div>
        </div>



        @include('profile.sidebarleft')

        <div class="col-md-8">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card"> 

                        <div class="card-header">All news</div>

                        <div class="container">
                        

                            <div class="row new_post">
                                <div class="col-md-12">
                                    <div id="app">

                                        <div>You can write a new post</div>
<!--                                        @{{msg}} <small>@{{con}}</small>-->
                                        <form method="post" enctype="multipart/form-data" >
                                            @csrf
                                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                            <textarea v-model='con' class="form-control new_mess" @keydown="inputHandler"></textarea>
                                        </form>

                                    </div>

                                </div>    
                            </div>


                            <div v-for="post in posts"> 

                                <div class="row data-user">                                

                                    <div class="col-md-2">
                                        <a :href="'http://one2one.com/profile/'+post.name"><img :src="'http://one2one.com/users/img/'+post.foto" class ='msg-foto'></a>
                                    </div>                               

                                    <div class="col-md-8">
                                        <span><b><a :href="'http://one2one.com/profile/'+post.name">@{{post.name}}</a></b></span>
                                        <span>Created: @{{post.created_at | myOwnTime}}</span>
                                    </div>

                                    <div class="col-md-2">
                                        <a href="#" data-toggle="dropdown" aria-haspopup="true" class="dell_btn"><i class="fa fa-cog"></i></a>

                                        <div class="dropdown-menu">
                                            <li><a>some action1</a></li>
                                            <li><a>some action1</a></li>                                            
                                            <li v-if="post.user_id == {{Auth::user()->id}}">
                                                <div class="dropdown-divider"></div>
                                                <a @click="deletePost(post.id)">
                                                    <i class="fa fa-trash"></i>delete</a>
                                            </li>
                                        </div>
                                    </div> 



                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>@{{post.content}}</p>                                       

                                    </div>                                    
                                </div>
                                
                                @if (Auth::check())
                                    
<!--                                    <div v-for="like in likes">
                                        
                                        <div v-if="post.id == like.post_id && like.user_id == {{Auth::user()->id}}">
                                            <div class="likeBtn" >
                                                <i class="fa fa-thumbs-up">&nbspLiked</i>
                                            </div>
                                        </div>
                                        
                                        <div v-else>
                                            <div class="likedBtn" @click="likePost(post.id)">
                                                <i class="fa fa-thumbs-up">&nbspLike</i>
                                            </div>
                                        </div>    
                                    
                                    </div>-->

                                <div v-if="post.user_id == {{Auth::user()->id}}">
                                            <div class="likeBtn" >
                                                <i class="fa fa-thumbs-up">&nbspLiked</i>
                                            </div>
                                        </div>   
                                @endif
                                <hr>

                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('profile.sidebarRight')    
    </div>
    @endsection
