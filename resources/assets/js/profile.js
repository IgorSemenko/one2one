require('./bootstrap');

window.Vue = require('vue');

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    
    el: '#app',
    
    data: {
        
        msg: 'Do you want to speak:',
        
        privateMsg: [],
        
        singleMsgs: [],
        
        msgFrom: '',
        
        conId: '',
        
        user2: ''
        
    },
    
    ready: function(){
        this.created();
    },
    
    created(){
        
       axios.get('http://one2one.com/getMessages')
                .then(response => {                   
                    console.log(response.data);
                    app.privateMsg = response.data;
                })
                .then(function(error){
                    console.log(error);
                });
       }, 
    
    
    methods : {
        
        messages: function(id){
                 app.msgFrom = '';
                 axios.get('http://one2one.com/getMessages/' + id)
                .then(response => {                   
                    console.log(response.data);
                    if(response.data !== 1){                        
                        app.singleMsgs = response.data;
                        app.conId = response.data[0].conversation_id;
                    } else {
                        app.singleMsgs = [];
                        app.conId = '';
                        app.user2 = id;
                    }
                })
                .then(function(error){
                    console.log(error);
                });
        },
        
        inputHandler(e){
            if(e.keyCode === 13 && !e.shiftKey){
                e.preventDefault();
                this.sendMsg();
            }
        },
        
        sendMsg(){
            if(this.msgFrom){
                axios.post('http://one2one.com/sendMsg', {
            
                    conId: this.conId,
                    msg: this.msgFrom,
                    user2: app.user2
                })
                    .then(function(response){                   
                        console.log(response);
                        
                        if(response.status == 200){
                     
                            app.singleMsgs = response.data;

                        } 
                          
                    })
                    .then(function(error){
                        console.log(error);
                    });
            }
        }
    }   
    
});

