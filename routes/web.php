<?php

use App\Like;
use App\Post;
use App\User;
use App\Profiles;

Auth::routes();

Route::get('facebook', function () {
    return view('facebook');
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function () {
    //return Auth::user()->test();
    return Post::with('likes')->get();
   
});


Route::get('/findFriends', 'ProfileController@findFriends');

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::post('addPost', 'PostsController@addPost');

Route::get('auth/facebook', 'Auth\FacebookController@redirectToFacebook');

Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');

Route::get('/posts', 'PostsController@index');

Route::post('/getPosts', 'PostsController@getPosts');

Route::get('likes', function(){
    return Like::all();
});

Route::middleware(['auth'])->group(function () {
    
    Route::get('/home', 'HomeController@index'); 

    Route::get('/profile/{user}', 'ProfileController@index');
    
    Route::get('/changeFoto', function(){
        return view ('profile.pic');
    });
    
    Route::post('/uploadFoto', 'ProfileController@uploadFoto');
    
    Route::get('editProfile', function(){
        
        if(Auth::check()){
            return view('profile.editProfile')->with('data', Auth::user()->profile);
        } else {
            return view('welcome');
        }       
        
    });
    
    Route::post('/updateProfile', 'ProfileController@updateProfile');
    
    Route::get('/addFriend/{id}', 'ProfileController@sendRequest');
    
    Route::get('/requests', 'ProfileController@requests');
    
    Route::get('/accepts', 'ProfileController@accepts');
    
    Route::get('/accept/{id}/{name}', 'ProfileController@accept');
    
    Route::get('/friends', 'ProfileController@friends');
    
    Route::get('/remove/{id}', 'ProfileController@requestRemove');
    
    Route::get('/removeFriend/{id}/{id2}', 'ProfileController@friendRemove');
    
    Route::get('/jobs', 'ProfileController@jobs');
    
    Route::get('/job/{id}', 'ProfileController@job');
    
    Route::get('/deletePost/{id}', 'PostsController@deletePost');
    
    Route::get('/likePost/{id}', 'PostsController@likePost');
});

//Private messages
Route::get('/getMessages', 'MessagerController@getMessages');

Route::get('/getMessages/{id}', 'MessagerController@getUserMessages');

Route::post('sendMsg', 'MessagerController@sendMsg');

Route::get('/messages', function(){
    return view('profile.messages');
});

//forgot password
Route::get('forgotPass', function(){
    return view('profile.forgotPass');
});

Route::post('/setToken', 'ProfileController@setToken');


Route::group(['prefix' => 'company', 'middleware' => ['auth' => 'Company']], function(){
    
    Route::get('/', 'CompanyController@index');
    
    Route::get('/addJob', function(){
        return view('company.addJob');
    });
    
    Route::post('/addJobSubmit', 'CompanyController@addJobSubmit');
    
    Route::get('/jobs', 'CompanyController@viewJobs');
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth' => 'Admin']], function(){
    
    Route::get('/', 'AdminController@index');
});

