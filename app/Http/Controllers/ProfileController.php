<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Friendships;
use App\Notification;
use App\User;



class ProfileController extends Controller
{   

    public function index($user){ 
        
        $userData = DB::table('users')
                ->leftJoin('profiles', 'profiles.users_id', 'users.id')
                ->where('name', $user)                
                ->get();
               
        return view('profile.index', compact('userData', $userData));
        
    }
    
    public function uploadFoto(Request $request){
        
        if($request){
            
            $file = $request->file('foto');

            $filename = $file->getClientOriginalName();

            $path = 'users/img';

            $file->move($path, $filename);       

            if(Auth::check()){
                $user_id = Auth::user()->id;
            } else {
                return view('welcome');
            }

            DB::table('users')->where('id', $user_id)->update(['foto' => $filename]);

            return back();        
            
          } else {
            
              return view('welcome');
              
          }
        
        
    }
    
    public function updateProfile(Request $request){ 
        
        if(Auth::check()){
            $user_id = Auth::user()->id;
        } else {
            return view('welcome');
        }
        
        DB::table('profiles')->where('users_id', $user_id)->update($request->except('_token'));
        
        return back();
    }
    
    public function findFriends(){
        
        if(Auth::user()){
            
            $uid = Auth::user()->id;
            
            $allusers = DB::table('users')
                    ->leftJoin('profiles', 'users.id', '=', 'profiles.users_id')
                    
                    ->where('users.id', '!=', $uid)
                    
                    ->get();

            return view('profile.findFriends', compact('allusers', $allusers)); 
            
        } else {
            return view('/welcome');
        }
    }
    
    public function sendRequest($id){
        
        Auth::user()->addFriend($id);
        
        return back();
    }
    
    public function requests(){
        
        if(Auth::check()){
            $uid = Auth::user()->id;
        } else {
            return view('welcome');
        }
        
        $friendrequests = DB::table('friendships')
                ->leftJoin('users', 'users.id', '=', 'friendships.requester')
                ->where('friendships.user_requested', '=', $uid)
                ->where('status', 0)
                ->get();
        
        return view('profile.requests', compact('friendrequests', $friendrequests));
    }
    
    public function accepts(){
        
        if(Auth::check()){
            $uid = Auth::user()->id;
        } else {
            return view('welcome');
        }
        
        $noti = DB::table('notifications')
                ->leftJoin('users', 'users.id', '=', 'notifications.user_logged')
                ->where('notifications.user_hero', '=', $uid)
                ->where('status', 1)
                ->get();
        
        DB::table('notifications')
                ->leftJoin('users', 'users.id', '=', 'notifications.user_logged')
                ->where('notifications.user_hero', '=', $uid)
                ->where('status', 1)
                ->update(['status' => 0]);
        
        return view('profile.notifications', compact('noti', $noti));
    }
    
    public function accept($id, $name){       
        
       if(Auth::check()){
            $uid = Auth::user()->id;
        } else {
            return view('welcome');
        };
       
       $checkrequest = friendships::where('requester', $id)
               ->where('user_requested', $uid)
               ->first();       
       
       if($checkrequest){
           
           $updatefriendship = DB::table('friendships')
                   ->where('user_requested', $uid)
                   ->where('requester', $id)
                   ->update(['status' => 1]);
           
           $notifications = new Notification;
           $notifications->note = 'accepted your friend request';
           $notifications->user_hero = $id;
           $notifications->user_logged= Auth::user()->id;
           $notifications->status = '1';
           $notifications->save();
           
           if($updatefriendship){
               
               return back()-> with('msg', 'You are now friend with ' . ucwords($name));
               
           } else {
               
               return back()-> with('msg', 'You are now friend with ' . ucwords($name));
           }
       }
       
    }
    
    public function friends(){
        
        if(Auth::check()){
            $uid = Auth::user()->id;
        } else {
            return view('welcome');
        }
        
        $friends1 = DB::table('friendships')
                ->leftJoin('users', 'users.id', 'friendships.user_requested')
                ->where('status', 1)
                ->where('requester', $uid)
                ->get();
        
        $friends2 = DB::table('friendships')
                ->leftJoin('users', 'users.id', 'friendships.requester')
                ->where('status', 1)
                ->where('user_requested', $uid)
                ->get();
        
        $friends = array_merge($friends1->toArray(), $friends2->toArray());
        
        return view('profile.friends', compact('friends', $friends));
    }
    
    public function requestRemove($id){
        
        if(Auth::check()){
            $uid = Auth::user()->id;
        } else {
            return view('welcome');
        }
        
         DB::table('friendships')
                 ->where('user_requested', $uid)
                 ->where('requester', $id)
                 ->delete();
         
         return back()->with('msg', 'Request has been deleted');
    }
    
    public function friendRemove($id, $idd){
        
               
        if(Auth::check()){
            $uid = Auth::user()->id;
        } else {
            return view('welcome');
        }
        
         DB::table('friendships')
                 ->where('user_requested', $uid)
                 ->where('requester', $id)
                 ->delete();
         
         DB::table('friendships')                 
                 ->where('user_requested', $idd)
                 ->where('requester', $uid)
                 ->delete();
         
         $name = User::where('id', $id)->get();
         
         //dd($name);
         
         return back()->with('msg',  ' has been deleted from your frienlist');
    }
    
    public static function cmp($a, $b) {

            if ($a['updated_at'] == $b['updated_at']) {
                return 0;
            }

            return strtotime($a['updated_at']) < strtotime($b['updated_at']) ? 1 : -1;
        }
        
    public function setToken(Request $req){
        dd($req->all());    
    }
    
    public function jobs(){
        
        $jobs = DB::table('jobs')->get();
        
        return view('profile.jobs', compact('jobs', $jobs));
    }
    
    public function job($id){
        
        $jobs = DB::table('jobs')
                ->where('id', $id)
                ->get();
        
        return view('profile.job', compact('jobs', $jobs));
    }   
   
}
