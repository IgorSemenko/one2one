@extends('profile.master')

@section('content')
<div class="container">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{url('/profile').'/'. Auth::user()->name}}">Profile</a></li>
            <li class="breadcrumb-item"><a href="{{url('/requests')}}">Requests to friends</a></li>
        </ol>
    </nav>

    <div class="row">
        
        @include('profile.sidebarleft')
        
        <div class="col-md-10">
            <div class="row justify-content-center">

                <div class="col-md-12">
                    
                    <div class="card">
                        
                        <div class="card-header">People who want to be your friend
                           
                        </div>
                        <div class="card-body">
                            <div class="row"> <h6 class="col-md-12">All users:</h6></div>
                            
                            @if(session()->has('msg'))
                            
                                <p class="alert alert-success">{{session()->get('msg')}}</p>
                            
                            @endif    
                            
                            @foreach($friendrequests as $user)
                            
                            <div class="col-md-4 find-user">
                                <div class="img-thumbnail"> 
                                <h3>{{ucwords($user->name)}}</h3>
                                    <img src="{{URL::asset('users/img/' . $user->foto)}}" alt='profile photo' height="140" width="120">
                                    <p><a class="btn-sm btn-info" href="{{url('/accept')}}/{{$user->requester}}/{{$user->name}}">Confirm</a></p>
                                    <p><a class="btn-sm btn-default" href="{{url('/remove')}}/{{$user->requester}}">Remove</a></p>
                                </div>
                                
                            </div>
                            <hr>
                            
                            @endforeach                           
                            
                        </div>
                        
                    </div>
                </div>

                
                </div>
                
                
            </div>
        </div>
    </div>
    @endsection
