@extends('profile.master')

@section('content')
<div class="container">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{url('/profile').'/'. Auth::user()->name}}">Profile</a></li>
            <li class="breadcrumb-item"><a href="{{url('/editProfile')}}">Edite profile</a></li>
        </ol>
    </nav>

    <div class="row">

        @include('profile.sidebarleft')

        <div class="col-md-10">
            <div class="row justify-content-center">

                <div class="col-md-12">                            
                    <div class="card">
                        <div class="card-header">{{ucwords( Auth::user()->name )}}</div>

                        <div class="card-body">
                            @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}                                
                                </div>
                                @endif

                                You can edit your profile!
                            </div>
                            <form action="{{url('/updateProfile')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}"/>  
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <img src="{{URL::asset('users/img/' . Auth::user()->foto)}}" alt='profile photo' height="120" width="100">
                                            <p><a href="{{URL('/')}}/changeFoto">Change Image</a></p>
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="row"> 
                                        <div class="col-md-12">
                                            <p><label>My city</label></p>
                                            <input type="text" name="city" value="{{$data->city}}" class="form-control-sm">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p><lable>About me</lable></p>
                                            <textarea style="resize: none; width: 100%; height: 100px;"name="about" placeholder="{{$data->about}}" class="form-control-sm"></textarea>
                                            <input type="submit" class="form-control-sm btn btn-info">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
