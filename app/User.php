<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\Traits\Friendable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Profiles;

class User extends Authenticatable {

    use Notifiable;

    use Friendable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'facebook_id', 'gender', 'age', 'foto'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function addNew($input) {
        $check = static::where('facebook_id', $input['facebook_id'])->first();


        if (is_null($check)) {
            return static::create($input);
        }


        return $check;
    }

    public function profile() {

        return $this->hasOne('App\Profiles', 'users_id');
    }

    public static function profilesUser() {
        
        return $this->hasOne('App\Profiles', 'users_id');
    }

    public function isRole() {
        return $this->role;
    }

}
