
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({

    el: '#app',

    data: {

        con: 'your post',

        msg: 'written:',

        posts: [],
        
        delete: '',
        
        likes: []        
        
    },

    ready: function () {
        this.created();
    },

    created() {

        axios.post('http://one2one.com/getPosts')
                .then(response => {
                    console.log(response);
                    this.posts = response.data;
                    Vue.filter('myOwnTime', function (value) {

                        return moment(value).fromNow();
                    })
                })
                .then(function (error) {
                    console.log(error);
                });
                
        axios.get('http://one2one.com/likes')
                .then(response => {
                    console.log(response);
                    this.likes = response.data;                    
                })
                .then(function (error) {
                    console.log(error);
                });        
    },

    methods: {

        addPost() {

            axios.post('http://one2one.com/addPost', {

                con: this.con
            })
                    .then(function (response) {
                        console.log(response);

                        if (response.status == 200) {

                            app.posts = response.data;

                        }
                    })
                    .then(function (error) {
                        console.log(error);
                    });
        },

        inputHandler(e) {
            if (e.keyCode === 13 && !e.shiftKey) {
                e.preventDefault();
                this.addPost();
            }
        },

        deletePost(id) {

             axios.get('http://one2one.com/deletePost/' + id)
                
                .then(response => {                   
                    console.log(response.data);
                    
                    if (response.status == 200) {

                            app.posts = response.data;

                        }
                })
                .then(function(error){
                    console.log(error);
                });
        },

        likePost(id) {

            axios.get('http://one2one.com/likePost/' + id)

                    .then(function (response) {
                        console.log(response);

                        if (response.status == 200) {

                            app.posts = response.data;

                        }
                    })
                    .then(function (error) {
                        console.log(error);
                    });
        }

    }

});

