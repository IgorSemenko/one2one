<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Friendships;
use App\Notification;
use App\User;
use App\Like;
use App\Http\Controllers\ProfileController;


class PostsController extends Controller {

    public function index() {
        
        if(Auth::check()){
            $uid = Auth::user()->id;
        } else {
            return view('welcome');
        }        

        $friends1 = DB::table('friendships')
                //->select('likes.post_id',  'likes.user_id', 'posts.id', 'posts.updated_at', 'users.id as main_id', 'name', 'requester',  'posts.user_id as postuserid', 'content', 'user_requested', 'likes.user_id as number')
                ->leftJoin('users', 'users.id', 'friendships.user_requested')
                ->leftJoin('posts', 'posts.user_id', 'users.id')
                ->leftJoin('likes', 'likes.post_id', 'posts.id')
                ->where('friendships.status', 1)
                ->where('requester', $uid)
                ->get();

        $friends2 = DB::table('friendships')
                //->select('likes.post_id',  'likes.user_id', 'posts.id','posts.updated_at', 'users.id as main_id', 'name', 'requester',  'posts.user_id as postuserid', 'content', 'user_requested', 'likes.user_id as number')
                ->leftJoin('users', 'users.id', 'friendships.requester')
                ->leftJoin('posts', 'posts.user_id', 'users.id')
                ->leftJoin('likes', 'likes.post_id', 'posts.id')
                ->where('friendships.status', 1)
                ->where('user_requested', $uid)
                ->get();
        
        $myPosts = DB::table('users')
                //->select('likes.post_id',  'likes.user_id', 'posts.id', 'posts.updated_at', 'users.id as main_id', 'name',  'posts.user_id as postuserid', 'content', 'likes.user_id as number')
                ->leftJoin('posts', 'users.id', 'posts.user_id')
                ->leftJoin('likes', 'likes.post_id', 'posts.id')
                ->where('posts.user_id', $uid)
                ->get();

        $posts = array_merge($friends1->toArray(), $friends2->toArray(), $myPosts->toArray());
       
        
        //Bubble sorted
        
        for($i=0; $i<count($posts); $i++){
            for($j = $i+1; $j<count($posts); $j++){
               if(strtotime($posts[$i]->updated_at) < strtotime($posts[$j]->updated_at)){                  
                   
                    $temp = $posts[$i];
                    $posts[$i] = $posts[$j];
                    if($i < count($posts)-1){   
                        $posts[$j] = $temp;
                    
                   }
               }
            }
        } 
        
        //dd(strtotime($posts[0]->updated_at));
        
        $likes = Like::all();
        
        return view('profile.posts', compact(['posts' => $posts, 'like' => $likes]));        
    }
    
    public function addPost(Request $request){
        
        $content = $request->con;
        
        $createPost = DB::table('posts')
                ->insert(['content' => $content, 'user_id' => Auth::user()->id, 
                        
                'status' => 0, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);  
        
        if($createPost){
            
            return self::getPosts();
        }
        
    }
    
     public function deletePost($id){        
        
        
        $deletePost = DB::table('posts')
                ->where('id', $id)
                ->delete();
        
        if($deletePost){
            
           return self::getPosts();
        
        }
        
    }
    
    public static function getPosts(){
        
        if(Auth::check()){
            $uid = Auth::user()->id;
        } else {
            return view('welcome');
        }

        $friends1 = DB::table('friendships')
                //->select('likes.post_id',  'likes.user_id', 'posts.id', 'posts.updated_at', 'users.foto', 'users.id as main_id', 'name', 'requester',  'posts.user_id as postuserid', 'content', 'user_requested', 'likes.user_id as number')
                ->leftJoin('users', 'users.id', 'friendships.user_requested')
                ->leftJoin('posts', 'posts.user_id', 'users.id')
                ->leftJoin('likes', 'likes.post_id', 'posts.id')
                ->where('friendships.status', 1)
                ->where('requester', $uid)
                ->get();

        $friends2 = DB::table('friendships')
                //->select( 'likes.post_id',  'likes.user_id','posts.id', 'posts.updated_at', 'users.foto', 'users.id as main_id', 'name', 'requester',  'posts.user_id as postuserid', 'content', 'user_requested', 'likes.user_id as number')
                ->leftJoin('users', 'users.id', 'friendships.requester')
                ->leftJoin('posts', 'posts.user_id', 'users.id')
                ->leftJoin('likes', 'likes.post_id', 'posts.id')
                ->where('friendships.status', 1)
                ->where('user_requested', $uid)
                ->get();
        
        $myPosts = DB::table('users')
                //->select('likes.post_id',  'likes.user_id', 'posts.id', 'posts.updated_at', 'users.foto', 'users.id as main_id', 'name',  'posts.user_id as postuserid', 'content', 'likes.user_id as number')
                ->leftJoin('posts', 'users.id', 'posts.user_id')
                ->leftJoin('likes', 'likes.post_id', 'posts.id')
                ->where('posts.user_id', $uid)
                ->get();

        $posts = array_merge($friends1->toArray(), $friends2->toArray(), $myPosts->toArray());
       
        
        //Bubble sorted
        
        for($i=0; $i<count($posts); $i++){
            for($j = $i+1; $j<count($posts); $j++){
               if(strtotime($posts[$i]->updated_at) < strtotime($posts[$j]->updated_at)){                  
                   
                    $temp = $posts[$i];
                    $posts[$i] = $posts[$j];
                    if($i < count($posts)-1){   
                        $posts[$j] = $temp;
                    
                   }
               }
            }
        }         
        
        return $posts;
    }
    
    public function likePost($id){
        
        $setLike = DB::table('likes')->Insert([
            'post_id' => $id,
            'user_id' => Auth::user()->id,
        ]);
        
        if($setLike){
            return self::getPosts();
        }
    }
}

